# GitDorker-API

This is a wrapper over the [GitDorker](https://github.com/obheda12/GitDorker) which is a tool that helps you dork Github. I strongly recommend against using this wrapper in a production environment as there are a lot of inefficiencies in this and the code isn't very maintainable.

## Dependencies

- Docker

## Build Process

- [Generate Github Personal Access](https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token) tokens and place one token per line in the `TOKENS_FILE`
- Build and run the docker container using docker-compose
  
  ```bash
  docker-compose up --build 
  ```

- The API runs in port `52121` by default, you can customise this by changing the value in `docker-compose.yml` file.
