from typing import Optional
from fastapi import FastAPI, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel, validator
from subprocess import CalledProcessError, TimeoutExpired, run
from os import path, remove
from uuid import uuid4
import csv

app = FastAPI()

class git_dorker_params(BaseModel):
    # keyword: Optional[list] = []
    # users: Optional[list] = []
    # organization: Optional[list] = []
    query: list
    # recentlyindexed: Optional[bool] = False
    # limitbypass: Optional[bool] = False
    # patternfilter: Optional[bool] = False
    # positiveresults: Optional[bool] = False

    @validator('query')
    def atleast_one_query(cls, value):
        assert len(value) > 0, "Please provide atleast one query"
        return value

@app.post("/")
def git_dorker_route(args: git_dorker_params):

    current_folder = path.split(path.abspath(__file__))[0]
    output_file = str(uuid4())

    # Limiting the number of threads as it doesn't seem to have any effect in the number of requests being sent
    arguments = f"--tokenfile TOKENS_FILE --output {output_file} --dorks ALL_DORKS --threads 1 --positiveresults --query {','.join(args.query)}"
    
    # if args.keyword:
    #     arguments = f"{arguments} --keyword {','.join(args.keyword)}"
    # if args.query:
    #     arguments = f"{arguments} --query {','.join(args.query)}"  
    # if args.users:
    #     arguments = f"{arguments} --users {','.join(args.users)}"
    # if args.organization:
    #     arguments = f"{arguments} --organization {','.join(args.organization)}"
    # if args.recentlyindexed:
    #     arguments = f"{arguments} --recentlyindexed"
    # if args.limitbypass:
    #     arguments = f"{arguments} --limitbypass"
    # if args.patternfilter:
    #     arguments = f"{arguments} --patternfilter"
    # if args.positiveresults:
    #     arguments = f"{arguments} --positiveresults"
    
    try:
        command = [f"python3 {path.join(current_folder,'gitdorker.py')} {arguments}"] # Use this to hide gitdorker output &> /dev/null"]
        run(command, timeout=18000, shell=True, executable='/bin/bash', check=True)
    except TimeoutExpired as err:
        return JSONResponse(status_code=status.HTTP_408_REQUEST_TIMEOUT, content={"error":"Gitdorker timed out"})
    except CalledProcessError as err:
        return JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, content={"error":err})
    
    try:
        gitdorker_output_file = f"{path.join(current_folder, f'{output_file}_gh_dorks.csv')}"
        f = open(gitdorker_output_file, "r")
        formatted_output = [i for i in csv.DictReader(f)]
        remove(gitdorker_output_file)
        return formatted_output
    except FileNotFoundError:
        return JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, content={"error":"Gitdorker results not found"})
