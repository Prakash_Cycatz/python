# Cron

The script that can be run as a cron job which will take all domains from a MYSQL table and call the API and insert the results to another MYSQL table.

## Dependencies

- Python 3.9

## Build Process

- Assign the value of `API_ENDPOINT` in `config.py` as the base url of API exposed using the steps in `/api`.
- Assign values to other variables in `config.py`
- Create a python virtual environment

```bash
python3 -m venv venv
```

- Activate the environment

```bash
source venv/bin/activate
```

- Install the dependencies
  
```bash
pip install -r cron_requirements.txt
```

- Run the script

```bash
python cron_client.py
```
