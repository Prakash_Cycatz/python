# from pymongo import MongoClient
import requests
import config
from mysql import connector

# client = MongoClient(config.MONGO_CONNECTION_STRING)
# mongo_database = client[config.MONGO_DATABASE]
# mongo_collection = mongo_database[config.RESULTS_COLLECTION]

db_connection = connector.connect(host=config.MYSQL_HOST, port=config.MYSQL_PORT, user=config.MYSQL_USERNAME, password=config.MYSQL_PASSWORD, database=config.MYSQL_DATABASE)
cursor = db_connection.cursor()


def call_api(endpoint, query):
    print(f"Calling API with parameters {query}")
    response = requests.post(endpoint, json=query)
    #print(response.json())
    if response.status_code/100 == 2:
        return response.json()
    else:
        raise Exception(response.text)


def get_domains_from_mysql():
    query = ("SELECT organization_code, domain_name from organization_code_domain_mapping;")
    cursor.execute(query)
    return [{"organization_code":organization_code, "domain_name":domain_name} for organization_code, domain_name in cursor]


def insert_into_mysql(results):
    try:
        print(f"Inserting data into MySQL {results}")
        add_row = ("INSERT INTO gitdorker_results (organization_code, dork_name, dork_url, number_of_results) VALUES (%(CODE)s, %(DORK)s, %(URL)s, %(NUMBER OF RESULTS)s);")
        cursor.execute(add_row, results)
        db_connection.commit()
    except Exception as err:
        print(f"ERROR {err}")
        exit(1)


# def insert_into_mongo(results):
#     try:
#         print(f"Inserting data into MongoDB {results}")
#         mongo_collection.insert_many(results)
#     except Exception as err:
#         print(err)
#         exit(1)


def main():
    try:
        code_domains = get_domains_from_mysql()
        print(f"Got domains {code_domains}")
        for code_domain in code_domains:
            results = call_api(config.API_ENDPOINT, {"query":[code_domain["domain_name"]]})
            for result in results:
                result["CODE"] = code_domain["organization_code"]
                #result["NUMBER_OF_RESULTS"] = result["NUMBER OF RESULTS"]
                insert_into_mysql(result)
        
        db_connection.commit()
        cursor.close()
        db_connection.close()
    except Exception as err:
        print(f"ERROR {err}")


if __name__ == "__main__":
    main()
