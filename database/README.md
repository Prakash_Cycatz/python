# Database

This is the MYSQL database schema based on what was discussed in the call. It is not normalised and no foreign key constraints are enforced anywhere. There is a database with sample data attached.

- organization_name_code_mapping

| Column Name       | Data Type    |
| ----------------- | ------------ |
| organization_name | varchar(500) |
| organization_code | varchar(100) |
| created_time      | timestamp    |

```sql
CREATE TABLE `organization_name_code_mapping` (
  `organization_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `organization_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`organization_name`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

- organization_code_domain_mapping

| Column Name       | Data Type    |
| ----------------- | ------------ |
| organization_code | varchar(100) |
| domain_name       | varchar(500) |
| created_time      | timestamp    |

```sql
CREATE TABLE `organization_code_domain_mapping` (
  `organization_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `domain_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

- gitdorker_results

| Column Name       | Data Type    |
| ----------------- | ------------ |
| organization_code | varchar(100) |
| dork_name         | varchar(100) |
| dork_url          | varchar(100) |
| number_of_results | int          |
| created_time      | timestamp    |

```sql
CREATE TABLE `gitdorker_results` (
  `organization_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dork_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dork_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `number_of_results` int NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```
