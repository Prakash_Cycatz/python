-- MySQL dump 10.13  Distrib 8.0.26, for macos11.3 (x86_64)
--
-- Host: localhost    Database: gitdorker
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gitdorker_results`
--

DROP TABLE IF EXISTS `gitdorker_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gitdorker_results` (
  `organization_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dork_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dork_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `number_of_results` int NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gitdorker_results`
--

LOCK TABLES `gitdorker_results` WRITE;
/*!40000 ALTER TABLE `gitdorker_results` DISABLE KEYS */;
INSERT INTO `gitdorker_results` VALUES ('INFY','access_token','https://github.com/search?q=infosys.com+access_token&type=Code',12,'2021-09-02 21:36:21'),('PYU','.mlab.com password','https://github.com/search?q=payu.in+.mlab.com+password&type=Code',2,'2021-09-02 21:36:29'),('PYU','access_key','https://github.com/search?q=payu.in+access_key&type=Code',36,'2021-09-02 21:36:29'),('PYU','access_token','https://github.com/search?q=payu.in+access_token&type=Code',4078,'2021-09-02 21:36:29'),('PYU','admin_user','https://github.com/search?q=payu.in+admin_user&type=Code',3,'2021-09-02 21:36:29');
/*!40000 ALTER TABLE `gitdorker_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization_code_domain_mapping`
--

DROP TABLE IF EXISTS `organization_code_domain_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organization_code_domain_mapping` (
  `organization_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `domain_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization_code_domain_mapping`
--

LOCK TABLES `organization_code_domain_mapping` WRITE;
/*!40000 ALTER TABLE `organization_code_domain_mapping` DISABLE KEYS */;
INSERT INTO `organization_code_domain_mapping` VALUES ('INFY','infosys.com','2021-09-02 20:43:36'),('PYU','payu.in','2021-09-02 20:43:36');
/*!40000 ALTER TABLE `organization_code_domain_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organization_name_code_mapping`
--

DROP TABLE IF EXISTS `organization_name_code_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organization_name_code_mapping` (
  `organization_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `organization_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`organization_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organization_name_code_mapping`
--

LOCK TABLES `organization_name_code_mapping` WRITE;
/*!40000 ALTER TABLE `organization_name_code_mapping` DISABLE KEYS */;
INSERT INTO `organization_name_code_mapping` VALUES ('Infosys','INFY','2021-09-02 20:42:02'),('PayU','PYU','2021-09-02 20:42:02');
/*!40000 ALTER TABLE `organization_name_code_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-03  3:13:31
